var http = require("http"),
fs = require("fs");
http.createServer(function(req,res){
	req.addListener('end',function(){
		if(req.url == '/'){
		fs.readFile("test.txt",'utf-8',function(error,data){
			res.writeHead(200,{
				'Content-Type':'text/plain'
			});
		data = parseInt(data) + 1;
		fs.writeFile('test.txt',data);
		res.end('This page was refreshed ' + data + ' times !');
		});
	}
	else{
		res.writeHead(404);
		res.end();
	}
	});
	req.resume();
}).listen(8080);