var http = require("http"),
url = require("url");
http.createServer(function(req,res){
	req.addListener('end',function(){
		var _get = url.parse(req.url,true).query;
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Here is your data:' + _get['data'] + _get['type']);
	});
	req.resume();
}).listen(8080);