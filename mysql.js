var http = require('http'), 
	mysql = require("mysql"); 
	 
var connection = mysql.createConnection({ 
	user: "root", 
	password: "007", 
	database: "mydb"
}); 

http.createServer(function (request, response) { 
	request.addListener('end', function () { 
		connection.query('SELECT * FROM student;', function (error, rows, fields) { 
			response.writeHead(200, { 
				'Content-Type': 'x-application/json' 
			}); 
			response.end(JSON.stringify(rows)); 
		}); 
	}); 
	request.resume();
}).listen(8080);